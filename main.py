from twisted.internet import reactor
from scrapy.crawler import Crawler
from scrapy.settings import Settings
from scrapy import log, signals
from kogexceptions import *
from spiders.RedditSpider import *
from spiders.CnnBabySpider import *
from spiders.WikiCategorySpider import *

import time
import os
import sys

selected_spider_name = ""
spider_nest = list()

spider_nest.append(RedditSpider())
spider_nest.append(CnnBabySpider())
spider_nest.append(WikiSpider())

if __name__ == "__main__":
    for arg in sys.argv:
        selected_spider_name = arg

    selected_spider_list = [iter_spider for iter_spider in spider_nest if iter_spider.name == selected_spider_name]
    selected_spider = None

    if len(selected_spider_list) > 1:
        raise SameNameError
    elif len(selected_spider_list) < 1:
        raise NoSpiderError
    else:
        selected_spider = selected_spider_list[0]

    TIMESTAMP = time.strftime("%y%m%d%H%M%S")
    EXPORT_SUFFIX = 'output/{}/{}-post-dump-{}.json'.format(selected_spider.local_dir, selected_spider.name, TIMESTAMP)
    local_dir = os.path.dirname(__file__)
    export_path = "file:///{}/{}".format(local_dir, EXPORT_SUFFIX)

    spiderNest = []
    settings = Settings()
    settings.set('FEED_FORMAT', 'json')
    settings.set('FEED_URI', export_path)
    settings.set('DEPTH_LIMIT', 1)

    settings.set('ITEM_PIPELINES',  {
        'pipelines.DebugDataPipeline': 0
    })

    crawler = Crawler(settings)
    crawler.signals.connect(reactor.stop, signal=signals.spider_closed)
    crawler.configure()
    crawler.crawl(selected_spider)
    crawler.start()
    log.start(loglevel='DEBUG', logstdout=False)
    reactor.run()