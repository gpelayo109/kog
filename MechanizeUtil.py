__author__ = 'Geryl'

import cookielib
import mechanize

AR_COOKIE_FILENAME = "cookies/ar.txt"

browser = mechanize.Browser()
cookie_jar = cookielib.LWPCookieJar()
try:
    cookie_jar.revert(AR_COOKIE_FILENAME)
except IOError:
    print "No cookie exist"

browser.set_cookiejar(cookie_jar)
# browser.set_all_readonly(False)
browser.set_handle_robots(False)
browser.set_handle_refresh(False)
browser.addHeaders = [('user-agent', ' Mozilla/5.0 (X11; U; Linux 1686; en-US; rv:1.9.2.3)'
                                     ' Gecko/2010423 Ubuntu/10.04 (lucid) Firefox/3.6.3')]
response = browser.open("")

for f in browser.forms():
    print f

browser.select_form(nr=0)
browser.form['username'] = ''
browser.form['password'] = ''
browser.submit()

cookie_jar.save(AR_COOKIE_FILENAME)