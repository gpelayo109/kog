__author__ = 'Geryl Pelayo'

import os
import datetime
import codecs

LOCAL_DIR = 'output/raw-html/'

def dump_data(text, name):
    if not os.path.exists(LOCAL_DIR):
        os.mkdir(LOCAL_DIR)
    date_prefix = str(datetime.datetime.now().time()).replace(':', '')
    with codecs.open('{}{}-{}.html'.format(LOCAL_DIR, date_prefix, name), 'w', encoding='UTF-8') as dump_file:
        dump_file.write(text)