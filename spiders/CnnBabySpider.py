from scrapy.spider import Spider
from scrapy.selector import Selector
from items import Article


class CnnBabySpider(Spider):
    name = 'baby-cnn-articles'
    allowed_domains = ['http://www.cnn.com/']
    start_urls = ['http://edition.cnn.com/2015/03/12/football/womens-football-france/index.html']

    def __init__(self):
        Spider.__init__(self, self.name)

    def parse(self, response):
        for article_data in response.xpath("//p[@class='zn-body__paragraph']"):
            article_selector = Selector(text=article_data.extract())
            article = Article()
            article['text'] = response.xpath("//p[@class='zn-body__paragraph']/text()").extract()
        yield article