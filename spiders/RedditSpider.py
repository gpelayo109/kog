from spiders import KogSpider
from items import Article


class RedditSpider(KogSpider):
    name = 'reddit-links'
    allowed_domains = ['www.reddit.com']
    start_urls = ['http://www.reddit.com/r/AskReddit']

    def __init__(self, domain=None, local_dir=None):
        KogSpider.__init__(self, self.name)

    def parse_items(self, response):
        KogSpider.parse_items(self, response)
        for post_data in response.xpath("//a[@class='title may-blank ']/text()"):
            article = Article()
            article['title'] = post_data.extract()
            yield article