from KogSpider import KogSpider
from items import WikiCats


class WikiSpider(KogSpider):
    name = 'wiki-categories'
    allowed_domains = ['en.wikipedia.org']
    start_urls = ['http://en.wikipedia.org/wiki/Main_Page']

    def __init__(self, domain=None, local_dir=None):
        self.local_dir = local_dir
        KogSpider.__init__(self, self.name)

    def parse_items(self, response):
        KogSpider.parse_items(self, response)
        wiki = WikiCats()
        wiki['title'] = response.xpath("//h1/text()").extract()
        wiki['url'] = response.url
        category_section = response.xpath("//div[@id='catlinks']/div/ul")
        wiki['categories'] = list()
        for post_data in category_section:
            wiki['categories'] += post_data.xpath("li/a/text()").extract()
        yield wiki