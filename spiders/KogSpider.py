__author__ = 'Geryl'
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.lxmlhtml import LxmlLinkExtractor


class KogSpider(CrawlSpider):
    item_count = 0
    rules = (Rule(LxmlLinkExtractor(), callback="parse_items", follow=True),)
    local_dir = ""

    def __init__(self, name, local_dir=None):
        self.local_dir = name
        CrawlSpider.__init__(self, name)

    def parse_items(self, response):
        self.item_count += 1

