__author__ = 'Geryl'

from scrapy import Item, Field

class Article(Item):
    url = Field()
    title = Field()
    text = Field()


class WikiCats(Item):
    title = Field()
    categories = Field()
    url = Field()

class RedditPost(Item):
    title = Field()