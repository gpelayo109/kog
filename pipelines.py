__author__ = 'Geryl'
import datetime, time
import os
from scrapy.settings import Settings


class DebugDataPipeline(object):
    def __init__(self):
        self.time_started = -1

    def open_spider(self, spider):
        self.time_started = datetime.datetime.now()

    def close_spider(self, spider):
        timestamp = time.strftime("%y%m%d%H%M%S")
        folder = '{}/output/{}/'.format(os.path.dirname(__file__), spider.local_dir)
        debug_filepath = '{}debug-{}.txt'.format(folder, timestamp)

        if not os.path.exists(folder):
            os.mkdir(folder)

        debug_file = open(debug_filepath, 'w')
        time_elapsed = "Time Elapsed: {}\n".format(str(datetime.datetime.now() - self.time_started))
        item_count = "Item Count: {}\n".format(str(spider.item_count))
        depth = "Depth: {}".format(str(Settings().get('DEPTH_LIMIT')))

        debug_file.write(time_elapsed)
        debug_file.write(item_count)
        debug_file.write(depth)
        debug_file.close()