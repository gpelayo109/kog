__author__ = 'Geryl'


class SameNameError(Exception):
    message = "There are multiple spiders with the same name."
    pass

class NoSpiderError(Exception):
    pass